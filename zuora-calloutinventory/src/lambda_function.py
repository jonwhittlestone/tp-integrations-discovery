# import logging
import os
import json
import requests
import logging
from functools import lru_cache
from dotenv import load_dotenv

logging.getLogger().setLevel(logging.INFO)


import boto3
from botocore.exceptions import ClientError


def is_running_in_aws():
    return "AWS_LAMBDA_FUNCTION_NAME" in os.environ


@lru_cache(maxsize=1)
def get_secret_credentials(env: str = "staging") -> dict:
    secret_name = f"/{env}/zuora/calloutinventorylambda/credentials"
    region_name = "eu-west-2"

    # Create a Secrets Manager client
    session = boto3.session.Session()
    client = session.client(service_name="secretsmanager", region_name=region_name)

    try:
        get_secret_value_response = client.get_secret_value(SecretId=secret_name)
    except ClientError as e:
        # For a list of exceptions thrown, see
        # https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
        raise e
    return json.loads(get_secret_value_response["SecretString"])


def load_secret_credentials():
    creds = {}
    for env in ["staging", "production"]:
        creds[env] = (
            get_secret_credentials(env)
            if is_running_in_aws()
            else get_local_secrets(env)
        )
    return creds


def get_local_secrets(env) -> dict:
    load_dotenv()
    return {
        "client_id": os.environ.get(f"{env.upper()}_CLIENT_ID", ""),
        "client_secret": os.environ.get(f"{env.upper()}_CLIENT_SECRET", ""),
    }


CREDENTIALS = load_secret_credentials()
config = {
    "staging": {
        "url": "https://rest.apisandbox.zuora.com",
        "client_id": CREDENTIALS["staging"].get("client_id", None),
        "client_secret": CREDENTIALS["staging"].get("client_secret", None),
        "communication_profile_name": "English UK profile (EN)",
    },
    "production": {
        "url": "https://rest.zuora.com",
        "client_id": CREDENTIALS["production"].get("client_id", None),
        "client_secret": CREDENTIALS["production"].get("client_secret", None),
        "communication_profile_name": "English UK profile (EN)",
    },
}


@lru_cache(maxsize=1)
def get_access_token(env) -> str:
    ENDPOINT = "/oauth/token"
    try:
        url = f"{config[env].get('url')}{ENDPOINT}"
        payload = f"client_id={config[env].get('client_id')}&client_secret={config[env].get('client_secret')}&grant_type=client_credentials"
        headers = {"Content-Type": "application/x-www-form-urlencoded"}
        response = requests.request("POST", url, headers=headers, data=payload)
        res = response.json().get("access_token")
        if not res:
            raise requests.exceptions.ConnectionError
        return res
    except requests.exceptions.ConnectionError:
        logging.info("failed to get_access_token. Are all env vars set?\n")


@lru_cache(maxsize=1)
def get_communication_profile_id(access_token: str, env: str) -> str:
    ENDPOINT = "/settings/communication-profiles"
    try:
        url = f"{config[env].get('url')}{ENDPOINT}"
        payload = f""
        headers = {"Authorization": f"Bearer {access_token}"}
        response = requests.request("GET", url, headers=headers, data=payload)
        c_profile = [
            c
            for c in response.json()
            if c.get("name") == config[env].get("communication_profile_name")
        ]
        return c_profile[0].get("id")
    except (requests.exceptions.ConnectionError, AttributeError, IndexError):
        logging.info("failed to get_communication_profile_id.")


def get_active_callouts(access_token, env, c_profile_id: str) -> list:
    active_callouts = []

    ENDPOINT = f"/settings/communication-profiles/{c_profile_id}/notifications"
    try:
        url = f"{config[env].get('url')}{ENDPOINT}"
        payload = f""
        headers = {"Authorization": f"Bearer {access_token}"}
        response = requests.request("GET", url, headers=headers, data=payload)
        active_callouts = [
            c for c in response.json() if c.get("active") and c.get("calloutOption")
        ]
        return active_callouts
    except (requests.exceptions.ConnectionError, AttributeError, IndexError):
        logging.info("failed to get_active_callouts.")


def lambda_handler(_event: dict, _context) -> dict:
    ret = {}
    for env in ["staging", "production"]:
        access_token = get_access_token(env)
        c_profile_id = get_communication_profile_id(access_token, env)
        ret[env]: list = get_active_callouts(access_token, env, c_profile_id)

    response_json = json.dumps(ret)
    return {
        "statusCode": 200,
        "body": response_json,
        "headers": {"Content-Type": "application/json"},
    }


if __name__ == "__main__":
    """Test invocation"""
    output = lambda_handler(None, None)
    data = json.loads(output.get("body"))
    print(data)
    with open(".output.json", "w") as json_file:
        json.dump(data, json_file, indent=4)

import os
from unittest import mock
import json
from http import HTTPStatus
import pytest
import responses
from src.lambda_function import (
    get_access_token,
    get_communication_profile_id,
    get_active_callouts,
)


@pytest.fixture()
def mocked_access_token():
    """Fixture that returns a static access token response."""
    with open("tests/resources/fixture_access_token.json") as f:
        return json.load(f)


@pytest.fixture()
def mocked_communication_profiles():
    """Fixture that returns a static communication profiles response."""
    with open("tests/resources/fixture_communication_profiles.json") as f:
        return json.load(f)


@pytest.fixture()
def mocked_notifications():
    """Fixture that returns a static notifications response."""
    with open("tests/resources/fixture_notifications.json") as f:
        return json.load(f)


@responses.activate
def test_mocked_get_access_token(mocked_access_token):
    url = f"https://rest.apisandbox.zuora.com/oauth/token"
    responses.add(responses.POST, url, json=mocked_access_token, status=HTTPStatus.OK)
    res: str = get_access_token("staging")
    if not res:
        assert False, "the access token string has not been returned"
    assert True


@responses.activate
def test_mocked_get_commiuniction_profile_id(mocked_communication_profiles):
    url = f"https://rest.apisandbox.zuora.com/settings/communication-profiles"
    responses.add(
        responses.GET, url, json=mocked_communication_profiles, status=HTTPStatus.OK
    )
    res: str = get_communication_profile_id("access_token", "staging")
    if not res:
        assert False, "the communication profile id string has not been returned"
    assert True


@responses.activate
def test_mocked_get_active_callouts(mocked_notifications):
    c_profile_id = "2c92c0f8516cc19e01517c0cf1c7605b"
    url = f"https://rest.apisandbox.zuora.com/settings/communication-profiles/{c_profile_id}/notifications"
    responses.add(responses.GET, url, json=mocked_notifications, status=HTTPStatus.OK)
    res: str = get_active_callouts("access_token", "staging", c_profile_id)
    if not res:
        assert False, "the communication profile id string has not been returned"
    assert True

# zuora-calloutinventory

Contained are usage instructions for provisioning an endpoint that responds with the callouts used for Zuora staging and production instances.

The deployment of serverless components is described below mostly with variations of `aws cloudformation`. It is an experiment to deconstruct Trustpilot's deployment via the GitHub action. 

Arguably a more frictionless way to manage this workflow would be to use AWS Serverless Application model. 

## End goal

Output from the Zuora settings API detailing active callouts

```bash
http POST $(aws cloudformation describe-stacks --stack-name zuora-calloutinventoryapigateway-staging-1 --query "Stacks[0].Outputs[?OutputKey=='InvokeURL'].OutputValue" --output text)

HTTP/1.1 200 OK
...
{
    "staging": [
        {
            "id": "2c92a0fc6851442801687043e891054c",
            ...
            "eventName": "Amendment Processed",
            ...
            "calloutOption": true,
            "calloutBaseUrl": "https://zuora-staging.tp-staging.com/staging/amendment-processed",
            ...
        }
        ...
    ],
    "production": [
        {
            "id": "2c92a0fc6851442801687043e891054c",
            "eventName": "Invoice Due",
            "calloutOption": true,
            "calloutBaseUrl": "https://zuora-prod.trustpilot.com/prod/zuora-reclaimdate",
            ...
        },
        ...
    ]

```

## Usage

### Development

```bash

# install
cd zuora-calloutinventory
python3 .venv venv
source .venv/bin/activate
pip install -r requirements.txt

# run it locally
python3 src/lambda_function.py

# unit test
pytest tests
```


### Prerequisites for provisioning

- `aws-cli` set up with programmatic access to account
- a versioned aws bucket in which to store the lambda
    - <details>
        <summary>Expand for code</summary>

        > note. Username is specifc to your use case.
        
        ```bash
        aws cloudformation create-stack --stack-name storage-for-single-user --template-body file://cloudformation/storage-for-single-user.yaml --parameters ParameterKey=Username,ParameterValue=devhowapped
        ```
        </details>
    - get the bucket ID
        - <details>
            <summary>Expand for code</summary>
            
            ```bash
            aws s3 ls | grep storage-for-single-user | awk -F' ' '{print $3}'
            storage-for-single-user-s3bucket-n7a7fpecimhe
            ```
        </details>
- A LambdaExecution role
    - <details>
        <summary>Expand for code</summary>

        ```bash
        aws cloudformation create-stack --stack-name role-for-lambda --template-body file://cloudformation/lambda-role.yaml --capabilities CAPABILITY_IAM
        ```
    </details>
    
    - Get the role ARN
        - <details>
            <summary>Expand for code</summary>

            ```bash
            aws cloudformation describe-stacks --stack-name role-for-lambda --query "Stacks[0].Outputs[?OutputKey=='LambdaExecutionRoleArn'].OutputValue" --output text 
            ```
        </details>

- 2 secrets in AWS secrets manager `credentials` json blobs
    - In the CloudFormation template, modify the version controlled temporary secret values with actual credentials

        ```diff
        # create-secrets-for-zuora-creds.yaml 

        -         SecretString: '{"client_id": "changeme-staging-clientid", "client_secret": "changeme-staging-clientsecret"}'
        +         SecretString: '{"client_id": "XXXXXXX", "client_secret": "XXXXXXX"}'

        -         SecretString: '{"client_id": "changeme-production-clientid", "client_secret": "changeme-production-clientsecret"}'
        +         SecretString: '{"client_id": "XXXXXXX", "client_secret": "XXXXXXX"}'
        ```
    - <details>
        <summary>Expand for code</summary>

        ```bash
        aws cloudformation create-stack --stack-name create-secret-for-zuora-creds --template-body file://cloudformation/create-secrets-for-zuora-creds.yaml
        ```
    </details>

- `httpie`
- `zip`


> This example adds the environment `staging` and version `1` to created resources.

### Step 1. Prepare the Lambda

1. Create a zip deployment package containing the `.py' handler file and the dependencies [source](https://docs.aws.amazon.com/lambda/latest/dg/python-package.html#python-package-create-dependencies)

    <details>
    <summary>Expand for code</summary>

    ```bash
    cd zuora-calloutinventory
    pip install -r requirements.txt --target ./package

    # add deps to zip
    cd package
    zip -r ../package.zip .

    # add handler to zip, resulting in flat dir structure
	cd ../src
	zip ../package.zip lambda_function.py
    ```
    </details>

2. Upload the .zip to the S3 bucket. 

    The bucket will be referenced by the CloudFormation for the Lambda in the next /step.

    You will also need the version. The code snippet below shows how to get this.

    <details>
    <summary>Expand for code</summary>

    ```bash
    cd ..
    aws s3 cp package.zip s3://$(aws s3 ls | grep storage-for-single-user | awk -F' ' '{print $3}')/package.zip

    # validate template
    aws cloudformation validate-template --template-body file://cloudformation/zuora-calloutinventorylambda.yaml
    ```
    </details>

### Step 2. Run the Lambda CloudFormation template to provision the Lambda

Create a new stack called `zuora-calloutinventorylambda-staging-1`

<details>
<summary>Expand for code</summary>

```bash
aws cloudformation create-stack --stack-name zuora-calloutinventorylambda-staging-1 --template-body file://cloudformation/zuora-calloutinventorylambda.yaml --parameters ParameterKey=Environment,ParameterValue=staging ParameterKey=ReleaseNumber,ParameterValue=1 ParameterKey=UniqueId,ParameterValue=1 ParameterKey=S3BucketId,ParameterValue=$(aws s3 ls | grep storage-for-single-user | awk -F' ' '{print $3}') ParameterKey=LambdaExecutionRoleArn,ParameterValue=$(aws cloudformation describe-stacks --stack-name role-for-lambda --query "Stacks[0].Outputs[?OutputKey=='LambdaExecutionRoleArn'].OutputValue" --output text)  ParameterKey=S3ObjectVersion,ParameterValue=$(aws s3api list-object-versions --bucket $(aws s3 ls | grep storage-for-single-user | awk -F' ' '{print $3}') --prefix package.zip --query 'Versions[?IsLatest].[VersionId]' --output text) --capabilities CAPABILITY_AUTO_EXPAND
```
</details>

### Step 3. Run the API Gateway CloudFormation template

This creates a stack for API Gateway: `zuora-calloutinventoryapigateway-staging-1`

<details>
<summary>Expand for code</summary>

```bash
aws cloudformation create-stack --stack-name zuora-calloutinventoryapigateway-staging-1 --template-body file://cloudformation/zuora-calloutinventoryapigateway.yaml --capabilities CAPABILITY_IAM --parameters ParameterKey=Environment,ParameterValue=staging ParameterKey=ReleaseNumber,ParameterValue=1 ParameterKey=UniqueId,ParameterValue=1
```
</details>

### Step 4. Get the API Gateway Endpoint (optional)

```bash
aws cloudformation describe-stacks --stack-name zuora-calloutinventoryapigateway-staging-1 --query "Stacks[0].Outputs[?OutputKey=='InvokeURL'].OutputValue" --output text
```

### Step 5. Test the endpoint

```bash
http POST $(aws cloudformation describe-stacks --stack-name zuora-calloutinventoryapigateway-staging-1 --query "Stacks[0].Outputs[?OutputKey=='InvokeURL'].OutputValue" --output text)
```

### Step 6. Cleanup

1. Delete the API Gateway
    ```bash
    aws cloudformation delete-stack --stack-name zuora-calloutinventoryapigateway-staging-1
    ```

2. Delete the Lambda

    ```bash
    aws cloudformation delete-stack --stack-name zuora-calloutinventorylambda-staging-1 
    ```
3. Delete the Lambda role

    ```bash
    aws cloudformation delete-stack --stack-name role-for-lambda
    ```

4. Delete the package in the S3 bucket

    ```bash
    aws s3 rm s3://$(aws s3 ls | grep storage-for-single-user | awk -F' ' '{print $3}')/package.zip
    
    ```

5. Delete the S3 bucket

    ```bash
    aws cloudformation delete-stack --stack-name s3-bucket-for-single-user
    ```


### Step 7. Update lambda code 

Update the code, add to the zip package and push up a new version to S3.

Get the updated version ID and modify the cloudformation stack with the new version.

1. Make a change to the code

```diff
# src/lambda_function.json
...
- VERSION = 3
+ VERSION = 4
...

```

2. Sanity check the change locally

```
python3 src/lambda_function.py

{'statusCode': 200, 'body': '{"status": "success", "message": "Lambda function from package v2 executed successfully", "data": {"key1": "val1", "key2": "val2"}}', 'headers': {'Content-Type': 'application/json'}
```

3. Update the zip

```bash
cd src; zip ../package.zip lambda_function.py;cd -
updating: lambda_function.py (deflated 46%)
```

3. Push to S3

```bash
aws s3 cp package.zip s3://$(aws s3 ls | grep storage-for-single-user | awk -F' ' '{print $3}')/package.zip
```

4. Get the latest VersionID (optional)

```bash
aws s3api list-object-versions --bucket $(aws s3 ls | grep storage-for-single-user | awk -F' ' '{print $3}') --prefix package.zip --query 'Versions[?IsLatest].[VersionId]' --output text
```

5. Run `update-function-code` to update the S3 Object version for `package.zip` used by the lambda

```bash
aws lambda update-function-code --function-name zuora-calloutinventory-staging-1 --s3-bucket $(aws s3 ls | grep storage-for-single-user | awk -F' ' '{print $3}') --s3-key package.zip --s3-object-version $(aws s3api list-object-versions --bucket $(aws s3 ls | grep storage-for-single-user | awk -F' ' '{print $3}') --prefix package.zip --query 'Versions[?IsLatest].[VersionId]' --output text)
```
